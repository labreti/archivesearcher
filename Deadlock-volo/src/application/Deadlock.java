package application;

public class Deadlock {
	static Posto[] volo = new Posto[10];

    public static void main(String[] args) throws InterruptedException {
    	volo[1] = new Posto("Alphonse");
    	volo[2] = new Posto("Gaston");
        new Thread(new Runnable() {
            public void run() { volo[1].scambia(volo,2); }
        }).start();
     Thread.sleep(50);
        new Thread(new Runnable() {
            public void run() { volo[2].scambia(volo,1); }
        }).start();
        Thread.sleep(500);
        System.out.format("%s %s%n",volo[1].getNome(), volo[2].getNome());
    }
}

class Posto {
	String nome;
	public Posto(String nome) { this.nome = nome; }
	public synchronized String getNome() { return nome; }
	public synchronized void setNome(String nome) { this.nome = nome; }
	public synchronized void scambia(Posto[] volo, int from) {
		System.out.format("%s pronto%n",nome);
		String n=volo[from].getNome();
		volo[from].setNome(nome);
		nome=n;
	}
}